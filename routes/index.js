var express = require('express');
var router = express.Router();
const userController = require("../controller/userController");




/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});




router.post('/registration', userController.userRegister);

router.post('/login', userController.userlogin);

router.post('/conversation', userController.conversation);

router.get('/conversationss/:userId',userController.conversationid);


router.post('/message', userController.message);

router.get('/messagess/:conversationId',userController.messageid);


router.get('/users/:userId',userController.getusers);

module.exports = router;
